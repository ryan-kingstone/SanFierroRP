stock ShowDialog(playerid, dialogid, style, caption[], info[], button1[], button2[], status)
{
	SetPVarInt(playerid, "DialogStatus", status);
	ShowPlayerDialog(playerid, dialogid, style, caption, info, button1, button2);
	return 1;
}

stock strcpyEx(dest[], const source[], maxlength=sizeof dest)
{
	strcat((dest[0] = EOS, dest), source, maxlength);
}