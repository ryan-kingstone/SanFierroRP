#define NEXT_PAGE_SLOT 50

//Dialog ID's
#define DIALOG_MAPOBJECTS 94
#define DIALOG_NEWOBJECT 95

#define ON_MAPOBJECTS_LIST 940
#define ON_MAPOBJECTS_NEW 950

#define ON_MAPOBJECTS_NONE 951
#define ON_MAPOBJECTS_CHOSE 952

#define MOS_ADJUSTING "mosAdjusting"
#define MOS_STATUS "mosStatus"

#define MAX_OBJECT 256

#define GetDynamicObjectModel(%0) Streamer_GetIntData(STREAMER_TYPE_OBJECT, %0, E_STREAMER_MODEL_ID)

enum _OBJECT
{
	ItemID,
	ObjectID,
	ObjectOwnerID,
	ObjectDataSize,
	ObjectDataText[48],
	Float:PosX,
	Float:PosY,
	Float:PosZ,
	Float:RotX,
	Float:RotY,
	Float:RotZ,
};

new ObjectData[MAX_OBJECT][_OBJECT],
    bool:ValidObject[MAX_BUSINESS],
    MapObject[MAX_OBJECT],
    Total_Objects_Created;

enum _OBJECTS_INFO
{
	modelID,
	modelName[64]
}

stock ObjectsInfo[][_OBJECTS_INFO] = {
	{19948, "RIGHT TURN ILLEGAL"},
	{19949, "LEFT TURN ILLEGAL"},
	{19950, "STRAIGHT ILLEGAL"},
	{19951, "SHARP LEFT TURN WARNING"},
	{19952, "SHARP RIGHT TURN WARNING"},
	{19953, "LEFT TURN WARNING"},
	{19954, "RIGHT TURN WARNING"},
	{19955, "LEFT TURN REQUIRED"},
	{19956, "RIGHT TURN REQUIRED"},
	{19957, "STRAIGHT AHEAD REQUIRED"},
	{19962, "CROSS WARNING"},
	{19963, "T-CROSS WARNING"},
	{19964, "TRAFFIC LIGHT WARNING"},
	{19965, "RED LIGHT WARNING"},
	{19966, "STOP"},
	{19967, "ONE WAY DO NOT ENTER"},
	{19968, "<-- NO PARKING"},
	{19969, "NO PARKING -->"},
	{19970, "<- ONE WAY INFORMATION"},
	{19971, "ONE WAY INFORMATION ->"},
	{19972, "ROAD CLOSED"},
	{19975, "WORK ZONE"},
	{19976, "YIELD"},
	{19978, "TOW ZONE"},
	{19979, "SPEED CAMERA"},
	{19980, "BLANK BIG GREEN"},
	{19981, "BLANK SMALL GREEN"},
	{19983, "10 SPEED LIMIT"},
	{19985, "20 SPEED LIMIT"},
	{19987, "30 SPEED LIMIT"},
	{19989, "40 SPEED LIMIT"},
	{19991, "50 SPEED LIMIT"},
	{19992, "55 SPEED LIMIT"},
	{11699, "CUSTOM SPEED LIMIT"},
	{19425, "SPEED HUMP"},
	{1257, "BUS STOP"},
	{2773, "AIRPORT LINE BOLLARD"},
	{3578, "WATERFRONT BARRIER"}
};

// Hooks
#include "YSI\y_hooks"

hook OnGameModeInit()
{
    SetTimer("LoadObjects", 2000, false);
	return 1;
}

forward LoadObjects();
public LoadObjects()
{
	new query[65+1];
	format(query, sizeof(query), "SELECT * FROM `mapped_objects`");
	mysql_tquery(mysql, query, "LoadAllObjects", "");
}

forward LoadAllObjects();
public LoadAllObjects()
{

	if(cache_num_rows())
	{
		for(new i = 0; i < cache_num_rows(); i++)
		{
			new bid = GetFreeMapObjectsSlot();
			ValidObject[bid] = true;

			cache_get_value_int(i, "ItemID", ObjectData[bid][ItemID]);
			cache_get_value_int(i, "ObjectID", ObjectData[bid][ObjectID]);
			cache_get_value_int(i, "ObjectOwnerID", ObjectData[bid][ObjectOwnerID]);

			// custom data
			cache_get_value_int(i, "ObjectDataSize", ObjectData[bid][ObjectDataSize]);
			cache_get_value(i, "ObjectDataText", ObjectData[bid][ObjectDataText], 48);

			// coordinates
			cache_get_value_float(i, "PosX", ObjectData[bid][PosX]);
			cache_get_value_float(i, "PosY", ObjectData[bid][PosY]);
			cache_get_value_float(i, "PosZ", ObjectData[bid][PosZ]);

			// rotation
			cache_get_value_float(i, "RotX", ObjectData[bid][RotX]);
			cache_get_value_float(i, "RotY", ObjectData[bid][RotY]);
			cache_get_value_float(i, "RotZ", ObjectData[bid][RotZ]);

            MapObject[bid] = CreateDynamicObject(ObjectData[bid][ObjectID], ObjectData[bid][PosX], ObjectData[bid][PosY], ObjectData[bid][PosZ], ObjectData[bid][RotX], ObjectData[bid][RotY], ObjectData[bid][RotZ]);

			if(ObjectData[bid][ObjectDataSize] != 0 && ObjectData[bid][ObjectDataText])
			{
				SetMapObjectData(MapObject[bid], ObjectData[bid][ObjectDataSize], ObjectData[bid][ObjectDataText]);
			}

			Total_Objects_Created++;
		}
	}
	printf("[MapObjects]: %d MapObjects have been successfully loaded from the database.", Total_Objects_Created);
	return 1;
}

stock CreateMapObject(_ObjectID, _ObjectOwnerID, Float:_PosX, Float:_PosY, Float:_PosZ, Float:_RotX, Float:_RotY, Float:_RotZ)
{
    new query[512];

	mysql_format(mysql, query, sizeof(query), "INSERT INTO `mapped_objects` (`ObjectID`, `ObjectOwnerID`, `PosX`, `PosY`, `PosZ`, `RotX`,`RotY`,`RotZ`) VALUES (%d, %d, %f, %f, %f, %f, %f, %f)", _ObjectID, _ObjectOwnerID, _PosX, _PosY, _PosZ, _RotX, _RotY, _RotZ);
	mysql_tquery(mysql, query, "OnAddMapObject", "ddffffff", _ObjectID, _ObjectOwnerID, _PosX, _PosY, _PosZ, _RotX, _RotY, _RotZ);

	return 1;
}

forward OnAddMapObject(mObjectID, mObjectOwnerID, Float:mPosX, Float:mPosY, Float:mPosZ, Float:mRotX, Float:mRotY, Float:mRotZ);
public OnAddMapObject(mObjectID, mObjectOwnerID, Float:mPosX, Float:mPosY, Float:mPosZ, Float:mRotX, Float:mRotY, Float:mRotZ)
{
	new bid = GetFreeMapObjectsSlot();
	ValidObject[bid] = true;

	ObjectData[bid][ItemID] = cache_insert_id();
    ObjectData[bid][ObjectID] = mObjectID;
    ObjectData[bid][ObjectOwnerID] = mObjectOwnerID;
    ObjectData[bid][PosX] = mPosX;
    ObjectData[bid][PosY] = mPosY;
    ObjectData[bid][PosZ] = mPosZ;
    ObjectData[bid][RotX] = mRotX;
    ObjectData[bid][RotY] = mRotY;
    ObjectData[bid][RotZ] = mRotZ;

    MapObject[bid] = CreateDynamicObject(mObjectID, mPosX, mPosY, mPosZ, mRotX, mRotY, mRotZ);

	Total_Objects_Created++;
	return 1;
}

stock DeleteMapObject(mItemID)
{
    new query[512];

	mysql_format(mysql, query, sizeof(query), "DELETE FROM `mapped_objects` WHERE `ItemID` = '%d'", mItemID);
	mysql_tquery(mysql, query, "OnDeleteMapObject", "d", mItemID);

	return 1;
}

forward OnDeleteMapObject(mItemID);
public OnDeleteMapObject(mItemID)
{
	new object = GetMapObjectBySQLId(mItemID),
		oid = GetMapObjectItemReference(mItemID);

	if(object != -1)
	{
		DestroyDynamicObject(object);
	}

	ValidObject[oid] = false;

	ObjectData[oid][ItemID] = 0;
	ObjectData[oid][ObjectID] = 0;
	ObjectData[oid][ObjectOwnerID] = 0;
	ObjectData[oid][PosX] = 0.0;
	ObjectData[oid][PosY] = 0.0;
	ObjectData[oid][PosZ] = 0.0;
	ObjectData[oid][RotX] = 0.0;
	ObjectData[oid][RotY] = 0.0;
	ObjectData[oid][RotZ] = 0.0;
}

stock UpdateMapObjectData(mItemID, mSize, mText[])
{
	new query[256];

	mysql_format(mysql, query, sizeof(query), "UPDATE `mapped_objects` SET `ObjectDataSize` = '%d', `ObjectDataText` = '%s' WHERE `ItemID` = '%d'", mSize, mText, mItemID);
	mysql_tquery(mysql, query, "OnMapObjectUpdate", "dds", mItemID, mSize, mText);
}

forward OnMapObjectUpdate(mItemID, mSize, mText[]);
public OnMapObjectUpdate(mItemID, mSize, mText[])
{
	new object = GetMapObjectBySQLId(mItemID);

	if(object != -1)
	{
		SetMapObjectData(object, mSize, mText);
	}
}

stock SetMapObjectData(mObject, mSize, mText[])
{
	SetDynamicObjectMaterialText(mObject, 3, mText, OBJECT_MATERIAL_SIZE_256x128, "Arial Black", mSize, 1, 0xFFFFFFFF, 0, OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
	return 1;
}

stock GetMapObjectBySQLId(_ItemID)
{
	for(new i = 0; i < MAX_OBJECT; i++)
	{
		if(ObjectData[i][ItemID] == _ItemID)
		{
			return MapObject[i];
		}
	}
	return -1;
}

stock GetMapObjectItemReference(_ItemID)
{
	for(new i = 0; i < MAX_OBJECT; i++)
	{
		if(ObjectData[i][ItemID] == _ItemID)
		{
			return i;
		}
	}
	return -1;
}

stock GetMapObjectModelName(model)
{
	new name[64];
	name = "[CUSTOM OBJECT]";
	for (new i = 0; i < sizeof(ObjectsInfo); i++)
	{
		if (model == ObjectsInfo[i][modelID])
		{
			// Also the definition of "strcpy"
			strcat((name[0] = '\0', name), ObjectsInfo[i][modelName]);
			//format(name, sizeof(name), "%s", ModelsInfo[i][modelName]);
		}
	}
	return name;
}

stock GetFreeMapObjectsSlot()
{
	for(new i = 1; i < sizeof(ValidObject); i ++)
	{
		if(!ValidObject[i]) return i;
	}
	return -1;
}

stock DisplayObjectsDialog(playerid, fromslot)
{
	new string[2048];
	for (new i = fromslot; i < fromslot + NEXT_PAGE_SLOT; i++)
	{
		if ( i < sizeof( ObjectsInfo ) )
		{
			format(string, sizeof(string), "%s(%d) {AAAAFF}%s\n", string, ObjectsInfo[i][modelID], ObjectsInfo[i][modelName]);
		}
		if ( i == ( fromslot + NEXT_PAGE_SLOT ) - 1 && i < sizeof( ObjectsInfo ) )
			strcat(string, "{FFFF00}Next Page\n");
	}
	SetPVarInt(playerid, "FromSlot", fromslot);
	ShowDialog(playerid, DIALOG_NEWOBJECT, DIALOG_STYLE_LIST, "Select your item", string, "Select", "Cancel", ON_MAPOBJECTS_NEW);
}

stock DisplayCurrentObjectsList(playerid)
{
	new dstr[2048];
	for (new i = 0; i < MAX_OBJECT; i++)
	{
        if(ValidObject[i]) {
            format(dstr, sizeof(dstr), "%s[{33FF33}%i{FFFFFF}] %s\n", dstr, ObjectData[i][ItemID], GetMapObjectModelName(ObjectData[i][ObjectID]));
        }
	}
	ShowDialog(playerid, DIALOG_MAPOBJECTS, DIALOG_STYLE_LIST, "Objects List", dstr, "Select", "Cancel", ON_MAPOBJECTS_LIST);
	return 1;
}

stock StartEditing(playerid, modelId)
{
	new mSlot = GetFreeMapObjectsSlot();

	SetPVarInt(playerid, MOS_ADJUSTING, 1);
	SendClientMessage(playerid, WHITE, "Press {FFFF00}ESC{FFFFFF} to cancel.");
	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);
	MapObject[mSlot] = CreateDynamicObject(modelId, x + 2, y + 2, z, 0.0, 0.0, 0.0);
	EditDynamicObject(playerid, MapObject[mSlot]);
}

public OnPlayerEditDynamicObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz)
{
	if(GetPVarInt(playerid, MOS_ADJUSTING) == 1) {
		switch(response) {
			case EDIT_RESPONSE_CANCEL:
			{
				SendClientMessage(playerid, LIRED, "Object editing canceled.");
				DestroyDynamicObject(objectid);

				// Set the adjusting state variable to 0
				SetPVarInt(playerid, MOS_ADJUSTING, 0);
			}
			case EDIT_RESPONSE_FINAL:
			{
				SendClientMessage(playerid, LIGREEN, "Object editing saved.");
				SetDynamicObjectPos(objectid, x, y, z);
				SetDynamicObjectRot(objectid, rx, ry, rz);

				CreateMapObject(GetDynamicObjectModel(objectid), Player[playerid][CharacterID], x, y, z, rx, ry, rz);

				// At last, remove the old object:
				DestroyDynamicObject(objectid);

				// Set the adjusting state variable to 0
				SetPVarInt(playerid, MOS_ADJUSTING, 0);

				Total_Objects_Created++;	
			}
		}
	}
    return 1;
}

#include <YSI\y_hooks>

hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	new status = GetPVarInt(playerid, "DialogStatus");
    if(dialogid == DIALOG_MAPOBJECTS) {
        switch (status)
		{
            case ON_MAPOBJECTS_LIST:
            {
                if(response)
                {
                    new i = listitem + 1;
                    
                    SetPlayerPos(playerid, ObjectData[i][PosX], ObjectData[i][PosY], ObjectData[i][PosZ] + 5);
                    SendClientMessage(playerid, LIGREEN, "Teleported to object.");
                }
            }
        }
    }

    if (dialogid == DIALOG_NEWOBJECT)
	{
		switch (status)
		{
			case ON_MAPOBJECTS_NEW:
			{
				if (response)
				{
					new slot = GetPVarInt(playerid, "FromSlot");
					if (listitem != NEXT_PAGE_SLOT)
					{
						new i = slot + listitem;
						StartEditing(playerid, ObjectsInfo[i][modelID]);
					}
					else
					{
						DisplayObjectsDialog(playerid, slot + NEXT_PAGE_SLOT);
					}
				}
				else
				{
                    SendClientMessage(playerid, -1, "Canceled.");
					DeletePVar(playerid, "FromSlot");
				}
			}
		}
	}
	return 1;
}

command(mapobjects, playerid, params[])
{

	DisplayCurrentObjectsList(playerid);
	return 1;
}

command(mos, playerid, params[])
{
	return cmd_mapobjects(playerid, params);
}

command(createmapobject, playerid, params[])
{
	new modelId;
	if(!sscanf(params, "i", modelId))
	{
		StartEditing(playerid, modelId);
	} else
	{
		if (GetPVarInt(playerid, MOS_ADJUSTING) == 1)
			return SendClientMessage(playerid, LIRED, "Please finish the current operation.");
		DisplayObjectsDialog(playerid, 0);
	}

	return 1;
}

command(cmo, playerid, params[])
{
	return cmd_createmapobject(playerid, params);
}

command(deletemapobject, playerid, params[])
{
	new itemId;

	if(!sscanf(params, "i", itemId))
	{
		SendClientMessage(playerid, LIRED, "Server: Deleting object..");
		DeleteMapObject(itemId);
	} else return SendClientMessage(playerid, WHITE, "Server: /deletemapobject [itemID]");

	return 1;
}

command(dmo, playerid, params[])
{
	return cmd_deletemapobject(playerid, params);
}

command(setobjectdata, playerid, params[])
{
	new itemId, size, text[48];

	if(!sscanf(params, "iis[48]", itemId, size, text))
	{
		new object = GetMapObjectBySQLId(itemId);

		if(object != -1)
		{
			UpdateMapObjectData(itemId, size, text);
			SendClientMessage(playerid, LIGREEN, "Server: Object data has been set.");
		} else return SendClientMessage(playerid, WHITE, "Server: Invalid object");
	} else return SendClientMessage(playerid, WHITE, "Server: /setobjectdata [object] [size] [text]");

	return 1;
}