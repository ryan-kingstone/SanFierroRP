#include "YSI\y_hooks"

hook OnPlayerSpawn(playerid)
{
	if(GetPlayerFaction(playerid) != 0)
	{
		if(Factions[GetPlayerFaction(playerid)][FactionSpawnX] != 0)
		{
			SetPlayerPos(playerid, Factions[GetPlayerFaction(playerid)][FactionSpawnX], Factions[GetPlayerFaction(playerid)][FactionSpawnY], Factions[GetPlayerFaction(playerid)][FactionSpawnZ]);
		} else {
			SetPlayerPos(playerid, -1951.0, 137.0, 26.0); // default spawn: train station
		}
	} else {
		SetPlayerPos(playerid, -1951.0, 137.0, 26.0); // default spawn: train station
	}
	return 1;
}

hook OnPlayerRequestClass(playerid, classid)
{
	SetTimerEx("Spawn", 10, false, "i", playerid);
	return 1;
}

forward Spawn(playerid);
public Spawn(playerid)
{
	SpawnPlayer(playerid);
}