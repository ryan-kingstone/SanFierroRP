stock GetPlayerID(name[])
{
    if (strlen(name) >= 24)
        return INVALID_PLAYER_ID;
    new Name[16];
    for(new i; i < MAX_PLAYERS; i++)
    {
        GetPlayerName(i, Name, sizeof Name);
        if(!strcmp(Name, name, true))
            return i;
    }
    return INVALID_PLAYER_ID;
}